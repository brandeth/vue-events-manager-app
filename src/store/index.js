import Vue from "vue";
import Vuex from "vuex";
import * as user from "@/store/modules/user";
import * as event from "@/store/modules/event";
import * as notification from "@/store/modules/notification";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    categories: [
      "sustainability",
      "nature",
      "animal welfare",
      "housing",
      "education",
      "food",
      "community"
    ]
  },
  mutations: {},
  actions: {},
  modules: {
    user,
    event,
    notification
  },
  getters: {
    catLength: state => {
      return state.categories.length;
    },
    getTodoById: state => id => {
      return state.todos.find(todo => todo.id === id);
    }
  }
});
